package com.shusharin.fanfictiontomorrow.requests.get_list

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetListIdWriteBooksTest : RequestTestBase() {
    private lateinit var mockRequest: GetListIdWriteBooks

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getListIdWriteBooks)
    }

    @Test
    fun sendRequestFake() {
        val listBookId = ListBookId()
        listBookId.booksId = arrayListOf(1)
        val listIdParams = ListIdWriteBooksParams(1)
        sendRequest(mockRequest, listIdParams, listBookId)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val hashMap = slotHashMapBook.captured
        assert(hashMap[bookDefaultResponse.id]!!.id == 1)
    }
}