package com.shusharin.fanfictiontomorrow.requests.utils.classes

import com.shusharin.fanfictiontomorrow.BaseTest
import com.shusharin.fanfictiontomorrow.requests.utils.model.LibraryBook
import io.mockk.mockk
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.RepeatedTest
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource

internal class ReadBookTest : BaseTest() {
    @RepeatedTest(10)
    fun `values less the boundaries`() {
        val idStatus = (Int.MIN_VALUE..0).random()
        createReadBookWithStatusIdOutOfBoundary(idStatus)
    }

    @RepeatedTest(10)
    fun `values more the boundaries`() {
        val idStatus = (6..Int.MAX_VALUE).random()
        createReadBookWithStatusIdOutOfBoundary(idStatus)
    }

    @Test
    fun `values within the boundaries`() {
        for (idStatus in 1..5) {
            createReadBookWithStatusIdInOfBoundary(idStatus)
        }
    }

    @ParameterizedTest(name = "ReadBook should return the same values are within the boundaries")
    @ValueSource(ints = [1, 2, 4, 5])
    fun `ReadBook should return the same values are within the boundaries`(idStatus: Int) {
        createReadBookWithStatusIdInOfBoundary(idStatus)
    }

    private fun createReadBookWithStatusIdInOfBoundary(idStatus: Int) {
        val libraryBook = LibraryBook()
        libraryBook.statusId = idStatus
        val readBook = ReadBook(libraryBook, mockk())
        assertEquals(idStatus, readBook.statusReadingBook.id)
    }

    @ParameterizedTest(name = "ReadBook should return 5 without the boundaries")
    @ValueSource(ints = [0, 6])
    fun `ReadBook should return 5 without the boundaries`(idStatus: Int) {
        createReadBookWithStatusIdOutOfBoundary(idStatus)
    }

    private fun createReadBookWithStatusIdOutOfBoundary(idStatus: Int) {
        val readBook = createReadBookWithStatus(idStatus)
        assertEquals(5, readBook.statusReadingBook.id)
    }

    private fun createReadBookWithStatus(idStatus: Int): ReadBook {
        val libraryBook = LibraryBook()
        libraryBook.statusId = idStatus
        return ReadBook(libraryBook, mockk())
    }

}