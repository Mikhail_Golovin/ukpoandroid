package com.shusharin.fanfictiontomorrow.requests.delete_queue

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class DeleteChapterTest : RequestTestBase() {
    private lateinit var mockRequest: DeleteChapter

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::deleteChapter)
    }

    @Test
    fun sendRequestFake() {
        val result = true
        val deleteChapterParams = mockk<DeleteChapterParams>()
        sendRequest(mockRequest, deleteChapterParams, result)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val resultResponse = slotBoolean.captured
        assert(resultResponse == result)
    }
}