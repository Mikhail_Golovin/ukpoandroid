package com.shusharin.fanfictiontomorrow.requests.add_single

import android.graphics.Bitmap
import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class AddCoverTest : RequestTestBase() {
    private lateinit var mockRequest: AddCover

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::addCover)
    }

    @Test
    fun sendRequestFake() {
        val idCover = 1
        val coverParams = mockk<CoverParams>()
        sendRequest(mockRequest, coverParams, idCover)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val idCoverResponse = slotInt.captured
        assert(idCoverResponse == idCover)
    }
}