package com.shusharin.fanfictiontomorrow.requests.change

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import io.mockk.mockk
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class ChangeBookTest : RequestTestBase() {
    private lateinit var mockRequest: ChangeBook

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::changeBook)
    }
    @Test
    fun sendRequestFake() {
        val idBook = 1
        val changeBookParams = mockk<ChangeBookParams>()
        sendRequest(mockRequest, changeBookParams, idBook)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        val idBookResponse = slotInt.captured
        assert(idBookResponse == idBook)
    }
}