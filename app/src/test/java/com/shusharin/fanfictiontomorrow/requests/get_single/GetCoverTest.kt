package com.shusharin.fanfictiontomorrow.requests.get_single

import com.shusharin.fanfictiontomorrow.requests.RequestTestBase
import io.mockk.every
import io.mockk.mockk
import okhttp3.ResponseBody
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.time.Duration

internal class GetCoverTest : RequestTestBase() {
    private lateinit var mockRequest: GetCover

    @BeforeEach
    fun setUp() {
        mockRequest = getRequest(mockkMyApiServices::getCover)
    }

    @Test
    fun sendRequestFake() {
        setReturnNotNullBitmap()
        val responseBody = mockk<ResponseBody>()
        every { responseBody.byteStream() } answers { mockk() }
        sendRequest(mockRequest, mockk(),responseBody)
        Assertions.assertTimeoutPreemptively(Duration.ofMinutes(1)) {
            do {
                Thread.sleep(10)
            } while (quantityActiveRequests != 0)
        }
        assertTrue(slotBitmap.isCaptured)
    }
}