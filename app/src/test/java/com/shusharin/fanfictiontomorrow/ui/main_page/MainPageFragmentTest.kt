package com.shusharin.fanfictiontomorrow.ui.main_page

import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.ui.parents.FragmentBaseRecycleTest
import com.shusharin.fanfictiontomorrow.requests.get_list.GetListIdBooksInSection
import io.mockk.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class MainPageFragmentTest : FragmentBaseRecycleTest() {
    private lateinit var mockMainPageFragment: MainPageFragment

    @BeforeEach
    fun setUp() {
        setReturnNotNullBitmap()
        mockMainPageFragment = spyk(MainPageFragment(), recordPrivateCalls = true)
        mockkBase(mockMainPageFragment)
        mockkRecycleViewer(mockMainPageFragment)
    }

    @Test
    fun onCreateView() {
        mockMainPageFragment.onCreateView(mockk(), mockk(), mockk())
    }

    @Test
    fun onStart() {
        mockkConstructor(GetListIdBooksInSection::class)
        every { constructedWith<GetListIdBooksInSection>().sendRequest(any<Sections>()) } just Runs
        mockMainPageFragment.onCreateView(mockk(), mockk(), mockk())
        mockMainPageFragment.onStart()
    }

    @Test
    fun onStartUser() {
        MainActivity.idUser = 3
        mockMainPageFragment.onCreateView(mockk(), mockk(), mockk())
        mockMainPageFragment.onStart()
        MainActivity.idUser = -1
    }
}