package com.shusharin.fanfictiontomorrow.ui.book

import android.widget.Button
import com.shusharin.fanfictiontomorrow.databinding.FragmentBookBinding
import com.shusharin.fanfictiontomorrow.ui.FragmentBaseTest
import io.mockk.every
import io.mockk.mockk
import io.mockk.mockkStatic
import io.mockk.spyk
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

internal class BookFragmentTest : FragmentBaseTest() {
    private lateinit var mockBookFragmentTest: BookFragment

    @BeforeEach
    fun setUp() {
        every { bundle.getInt(any()) } returns 3
        setReturnNotNullBitmap()
        mockBookFragmentTest = spyk(BookFragment(), recordPrivateCalls = true)
        mockkBase(mockBookFragmentTest)

        mockkStatic(FragmentBookBinding::class)
        every {
            FragmentBookBinding.inflate(any(), any(), any())
        } answers { mockk() }
        val binding = mockk<FragmentBookBinding>()
        every { mockBookFragmentTest["setClipToOutline"]() } answers {}
        every { mockBookFragmentTest["setOnClickRead"]() } answers {}
        every { mockBookFragmentTest["setOnClickTableOfContent"]() } answers {}
        every { mockBookFragmentTest["setOnClickNameAuthor"]() } answers {}
        every { mockBookFragmentTest["setOnClickChange"]() } answers {}
        every { mockBookFragmentTest["setOnClickNameGenre"]() } answers {}
        every { mockBookFragmentTest["setOnClickList"]() } answers {}
        every { mockBookFragmentTest["getButtonList"]() } returns mockk<Button>()
        every {
            mockBookFragmentTest getProperty "binding"
        } propertyType FragmentBookBinding::class answers {
            binding
        }
    }

    @Test
    fun onCreateView() {
        mockBookFragmentTest.onCreateView(mockk(), mockk(), mockk())
    }

    @Test
    fun onChangeValue() {
        mockBookFragmentTest.onCreateView(mockk(), mockk(), mockk())
        mockBookFragmentTest.onChangeValue()
    }
}