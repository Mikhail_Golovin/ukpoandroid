package com.shusharin.fanfictiontomorrow.ui

import android.content.Context
import android.content.res.Resources
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.activity.OnBackPressedDispatcher
import androidx.core.view.MenuProvider
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewbinding.ViewBinding
import com.shusharin.fanfictiontomorrow.BaseTest
import com.shusharin.fanfictiontomorrow.databinding.FragmentRecyclerViewBinding
import com.shusharin.fanfictiontomorrow.ui.parents.MyViewFragment
import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import io.mockk.*
import org.junit.jupiter.api.TestInstance

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
open class FragmentBaseTest : BaseTest() {
    protected var isRefreshing = false
    protected val menuProviders = mutableListOf<MenuProvider>()
    val textView = spyk(TextView(mockk()))
    val imageView = spyk(ImageView(mockk()))
    val button = spyk(Button(mockk()))
    lateinit var recyclerView: RecyclerView
    val bundle = mockk<Bundle>()
    override var isMockAll = true
    protected fun <T : ViewModelUpdatable, T1 : ViewBinding> mockkBase(fragment: MyViewFragment<T, T1>) {
        MainActivity.toolbar = mockk()
        every { MainActivity.toolbar.title = any() } answers {}
        mockkSwipeRefreshLayout(fragment)
        mockkViewBinding(fragment)
        mockkActivity(fragment)
        mockkContext(fragment)
        mockkViewLifecycleOwner(fragment)
        mockkFragmentManager(fragment)
    }

    protected fun <T : ViewModelUpdatable, T1 : ViewBinding> mockkSwipeRefreshLayout(fragment: MyViewFragment<T, T1>) {
        val swipeRefreshLayout = mockk<SwipeRefreshLayout>()
        every { MainActivity.api } returns mockkMyApiServices
        every { swipeRefreshLayout.isRefreshing } answers { isRefreshing }
        every {
            swipeRefreshLayout.isRefreshing = any()
        } propertyType Boolean::class answers { isRefreshing = value }
        every { swipeRefreshLayout.setOnRefreshListener(any()) } answers {
            val run = it.invocation.args[0] as SwipeRefreshLayout.OnRefreshListener
            run.onRefresh()
        }
        every {
            fragment getProperty "swipeRefreshLayout"
        } propertyType SwipeRefreshLayout::class answers {
            swipeRefreshLayout
        }

        every {
            fragment setProperty "swipeRefreshLayout" value any<SwipeRefreshLayout>()
        } propertyType SwipeRefreshLayout::class answers {
            println("set swipeRefresh")
        }

        every {
            fragment["setSwipeRefreshLayout"]()
        } answers {}
    }

    protected fun <T : ViewModelUpdatable, T1 : ViewBinding> mockkViewBinding(fragment: MyViewFragment<T, T1>) {
        every {
            fragment["getRoot"]()
        } answers { mockk<SwipeRefreshLayout>() }

        mockkConstructor(View::class)
        mockkConstructor(RecyclerView::class)
        every { anyConstructed<View>().setOnClickListener(any()) } answers {}
        every { anyConstructed<View>().setOnClickListener { } } answers {}
        every { anyConstructed<View>().context } answers { mockk() }
        every { anyConstructed<RecyclerView>().context } answers { mockk() }
        every { anyConstructed<RecyclerView>().isScrollContainer = any() } answers { mockk() }
        every { anyConstructed<RecyclerView>().isFocusableInTouchMode = any() } answers { mockk() }
        recyclerView = mockk()
        mockkConstructor(ImageView::class)
        every { imageView.setOnClickListener(any()) } answers {}
        every { imageView.setImageBitmap(any()) } answers {}
        every { textView.setOnClickListener(any()) } answers {}
        every { textView.text = any() } answers {}
        every { button.setOnClickListener(any()) } answers {}
        every { button.text = any() } answers {}
        every { recyclerView.adapter = any() } answers {}
    }

    protected fun <T : ViewModelUpdatable, T1 : ViewBinding> mockkRecycleViewer(fragment: MyViewFragment<T, T1>) {


        every { recyclerView.setHasFixedSize(any()) } answers {}
        every {
            fragment getProperty "list"
        } propertyType RecyclerView::class answers {
            recyclerView
        }


        mockkStatic(FragmentRecyclerViewBinding::class)
        every {
            FragmentRecyclerViewBinding.inflate(any(), any(), any())
        } answers { mockk() }
        val binding = mockk<FragmentRecyclerViewBinding>()
        every {
            fragment getProperty "binding"
        } propertyType FragmentRecyclerViewBinding::class answers {
            binding
        }

        every {
            fragment["setList"]()
        } answers {}

        every {
            fragment["getRoot1"]()
        } answers { mockk<SwipeRefreshLayout>() }
    }

    protected fun <T : ViewModelUpdatable, T1 : ViewBinding> mockkActivity(fragment: MyViewFragment<T, T1>) {
        val activity = mockk<FragmentActivity>()
        every { activity.addMenuProvider(any()) } answers {
            val value = it.invocation.args[0] as MenuProvider
            menuProviders.add(value)
        }
        every { fragment["appBar"]() } answers {}
        every {
            fragment.requireActivity()
        } returns activity
        every {
            fragment.activity
        } returns activity
        every {
            activity.resources
        } returns mockk()

        val onBackPressedDispatcher = mockk<OnBackPressedDispatcher>()
        every {
            onBackPressedDispatcher.addCallback(any())
        } answers {}

        every {
            onBackPressedDispatcher.addCallback(any(), any())
        } answers {}

        every {
            activity.onBackPressedDispatcher
        } returns onBackPressedDispatcher

        every {
            fragment.getString(any())
        } returns "ID"

        every {
            fragment.arguments
        } returns bundle
    }

    protected fun <T : ViewModelUpdatable, T1 : ViewBinding> mockkContext(fragment: MyViewFragment<T, T1>) {
        val context = mockk<Context>()
        val resources = mockk<Resources>()
        every {
            fragment.requireContext()
        } returns context
        every {
            fragment.context
        } returns context
        every {
            context.resources
        } returns resources
    }

    protected fun <T : ViewModelUpdatable, T1 : ViewBinding> mockkFragmentManager(fragment: MyViewFragment<T, T1>) {
        val fragmentManager = mockk<FragmentManager>()
        every {
            fragment.parentFragmentManager
        } returns fragmentManager
    }

    protected fun <T : ViewModelUpdatable, T1 : ViewBinding> mockkViewLifecycleOwner(fragment: MyViewFragment<T, T1>) {
        val viewLifecycleOwner = mockk<LifecycleOwner>()
        every {
            fragment.viewLifecycleOwner
        } returns viewLifecycleOwner
    }

}