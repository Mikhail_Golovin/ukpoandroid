package com.shusharin.fanfictiontomorrow.requests.get_list

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.IGetSearchBook
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.FindBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import com.shusharin.fanfictiontomorrow.requests.utils.model.SearchParamsClass
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call

class ListIdSearchBooksParams(val searchParamsClass: SearchParamsClass) : MyRequestParams() {
    override fun toString(): String {
        return "ListIdSearchBooksParams(searchParamsClass=$searchParamsClass)"
    }
}

class GetListIdSearchBooks(
    override val owner: LifecycleOwner,
    override val api: MyApiServices,
    startRefresh: () -> Unit, stopRefresh: () -> Unit,

    ) : GetListIdBook<ListIdSearchBooksParams, FindBook>(
    startRefresh,
    stopRefresh
),
    IGetSearchBook {
    val liveSearchBook: MutableLiveData<HashMap<Int, FindBook>>
        get() = liveData

    fun sendRequest(searchParamsClass: SearchParamsClass) {
        data.clear()
        sendRequest(ListIdSearchBooksParams(searchParamsClass))
    }

    override fun analysisOfResponseBody(responseBody: ListBookId, params: ListIdSearchBooksParams) {
        getSearchBook(idUser, responseBody, params.searchParamsClass.sortingType)
    }

    override fun call(params: ListIdSearchBooksParams): Call<ListBookId> {
        val requestBody = RequestBody.create(
            MediaType.get("application/json; charset=utf-8"), params.searchParamsClass.toString()
        )
        return service.search(requestBody)
    }

    override fun setSearchBook(it: FindBook) {
        setLiveData(it)
    }
}