package com.shusharin.fanfictiontomorrow.requests.add_single

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestIdentity
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.model.BookClass
import okhttp3.MediaType
import okhttp3.RequestBody
import retrofit2.Call

class BookParams(val bookClass: BookClass) : MyRequestParams() {
    override fun toString(): String {
        return "BookParams(bookClass=$bookClass)"
    }
}

class AddBook(
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : MyRequestIdentity<Int, BookParams>(startRefresh, stopRefresh) {
    val liveIdBook: MutableLiveData<Int>
        get() = liveData

    fun sendRequest(bookClass: BookClass) {
        sendRequest(BookParams(bookClass))
    }

    override fun call(params: BookParams): Call<Int> {
        val requestBody = RequestBody.create(
            MediaType.get("application/json; charset=utf-8"), params.bookClass.toString()
        )
        return service.addBook(requestBody)
    }
}