package com.shusharin.fanfictiontomorrow.requests

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.utils.RetrofitServices
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.random.Random.Default.nextLong

const val DELAY_OF_REPEAT_REQUEST = 1000
const val MIN_DELAY_OF_REPEAT_REQUEST = 100
const val DELAY_OF_UPDATE_REQUEST = 10000

abstract class MyRequestParams {
    abstract override fun toString(): String
}

abstract class MyRequestIdentity<RequestType, RequestParams>(
    startRefresh: () -> Unit,
    stopRefresh: () -> Unit,
) : MyRequest<RequestType, RequestParams, RequestType>(startRefresh, stopRefresh) {
    override fun analysisOfResponseBody(responseBody: RequestType, params: RequestParams) {
        setLiveData(responseBody)
    }
}
var isTest = false
abstract class MyRequest<RequestType, RequestParams, RequestLiveData>(
    private val startRefresh: () -> Unit,
    private val stopRefresh: () -> Unit,
) {

    private val isUpdatable: Boolean = false
    private val isRepeatIfError: Boolean = true
    val liveData = MutableLiveData<RequestLiveData>()
    protected val service: RetrofitServices
        get() = MainActivity.mService

    protected val idUser: Int
        get() = MainActivity.idUser

    protected fun sendRequest(request: Call<RequestType>, params: RequestParams, count: Int = 20) {
        try {
            startRefresh()
            println("sendRequest $params")
            request.clone().enqueue(object :
                Callback<RequestType> {
                override fun onFailure(call: Call<RequestType>, t: Throwable) {
                    stopRefresh()
                    t.printStackTrace()
                    repeat(request, params, count)
                }

                override fun onResponse(
                    call: Call<RequestType>,
                    response: Response<RequestType>,
                ) {
                    val responseBody = response.body()
                    if (responseBody != null) {
                        analysisOfResponseBody(responseBody, params)
                        try {
                            update(request, params)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                        stopRefresh()
                    } else {
                        repeat(request, params, count)
                    }

                }

                override fun toString(): String {
                    stopRefresh()
                    return "Mock call $params"
                }
            })
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun update(request: Call<RequestType>, params: RequestParams) {
        if (isUpdatable) {
            Handler(Looper.getMainLooper()).postDelayed({
                print("update $params ")
                sendRequest(request, params)
            }, DELAY_OF_UPDATE_REQUEST.toLong())
        }
    }

    private fun repeat(request: Call<RequestType>, params: RequestParams, count: Int) {
        val countNext = count - 1
        if (count <= 0) return
        if (isRepeatIfError) {
            println("repeat $count $params")
            Handler(Looper.getMainLooper()).postDelayed(
                {
                    sendRequest(request, params, countNext)
                    stopRefresh()
                },
                nextLong(
                    MIN_DELAY_OF_REPEAT_REQUEST.toLong(),
                    DELAY_OF_REPEAT_REQUEST.toLong()
                )
            )
        }
    }

    fun sendRequest(params: RequestParams) {
        sendRequest(call(params), params)
    }

    protected fun setLiveData(responseBody: RequestLiveData) {
        liveData.value = responseBody!!
    }

    protected abstract fun analysisOfResponseBody(responseBody: RequestType, params: RequestParams)

    abstract fun call(params: RequestParams): Call<RequestType>

}