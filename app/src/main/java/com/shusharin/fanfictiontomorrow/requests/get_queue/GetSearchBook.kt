package com.shusharin.fanfictiontomorrow.requests.get_queue

import android.content.res.Resources
import android.graphics.Bitmap
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.IGetCover
import com.shusharin.fanfictiontomorrow.requests.IGetUser
import com.shusharin.fanfictiontomorrow.requests.MyRequest
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.FindBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.Person
import com.shusharin.fanfictiontomorrow.requests.utils.model.SearchBook
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import retrofit2.Call

class SearchBookParams(val idUser: Int, val idBook: Int, val sortType: Int) :
    MyRequestParams() {
    override fun toString(): String {
        return "SearchBookParams(idUser=$idUser, idBook=$idBook, sortType=$sortType)"
    }
}

class GetSearchBook(
    override val owner: LifecycleOwner,
    override val api: MyApiServices,
    val resources: Resources,
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) :
    MyRequest<SearchBook, SearchBookParams, FindBook>(startRefresh, stopRefresh),
    IGetUser,
    IGetCover {
    private var findBook = FindBook(resources)
    val liveFindBook: MutableLiveData<FindBook>
        get() = liveData

    fun sendRequest(idUser: Int, idBook: Int, sortType: Int) {
        sendRequest(SearchBookParams(idUser, idBook, sortType))
    }

    override fun analysisOfResponseBody(
        responseBody: SearchBook,
        params: SearchBookParams,
    ) {
        if (!responseBody.isDraft) {
            println("check $responseBody")
            findBook = FindBook(responseBody, resources)
            findBook.id = params.idBook

            updateLiveData()
            getUser(responseBody.authorId)
            getCover(responseBody.coverId)
        }
    }

    override fun setAuthor(it: Person) {
        liveFindBook.value!!.author = it
        updateLiveData()
    }

    override fun setCover(it: Bitmap) {
        liveFindBook.value!!.bitmap = it
        updateLiveData()
    }

    private fun updateLiveData() {
        setLiveData(findBook)
    }

    override fun call(params: SearchBookParams): Call<SearchBook> {
        return service.getSearchBook(params.idBook, params.sortType)
    }

}