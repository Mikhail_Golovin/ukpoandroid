package com.shusharin.fanfictiontomorrow.requests.utils.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize

class ListBookId {
    @JsonDeserialize(`as` = ArrayList::class, contentAs = Int::class)
    var booksId: ArrayList<Int> = ArrayList()

}