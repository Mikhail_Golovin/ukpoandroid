package com.shusharin.fanfictiontomorrow.requests.get_queue

import android.content.res.Resources
import android.graphics.Bitmap
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.IGetCover
import com.shusharin.fanfictiontomorrow.requests.IGetUser
import com.shusharin.fanfictiontomorrow.requests.MyRequest
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.ReadBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.LibraryBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.Person
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import retrofit2.Call

class LibraryBookParams(val idUser: Int, val idBook: Int) :
    MyRequestParams() {
    override fun toString(): String {
        return "LibraryBookParams(idUser=$idUser, idBook=$idBook)"
    }
}

class GetLibraryBook(
    override val owner: LifecycleOwner,
    override val api: MyApiServices,
    val resources: Resources,
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) :
    MyRequest<LibraryBook, LibraryBookParams, ReadBook>(
        startRefresh, stopRefresh
    ), IGetUser,
    IGetCover {
    private var libraryBook = ReadBook(resources)
    val liveLibraryBook: MutableLiveData<ReadBook>
        get() = liveData

    fun sendRequest(idUser: Int, idBook: Int) {
        sendRequest(LibraryBookParams(idUser, idBook))
    }

    override fun analysisOfResponseBody(
        responseBody: LibraryBook,
        params: LibraryBookParams,
    ) {
        if (!responseBody.isDraft) {
            libraryBook =
                ReadBook(
                    responseBody,
                    resources
                )
            libraryBook.id = params.idBook

            setLiveData(libraryBook)
            if (idUser != -1) {
                getUser(responseBody.authorId)
                getCover(responseBody.coverId)
            }
        }
    }

    override fun call(
        params: LibraryBookParams,
    ): Call<LibraryBook> {
        return service.getLibraryBook(params.idUser, params.idBook)
    }

    override fun setAuthor(it: Person) {
        libraryBook.author = it
        setLiveData(libraryBook)
    }

    override fun setCover(it: Bitmap) {
        libraryBook.bitmap = it
        setLiveData(libraryBook)
    }

}