package com.shusharin.fanfictiontomorrow.requests.utils.model

import com.fasterxml.jackson.databind.annotation.JsonDeserialize
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genre

class ListPresentBookInGenre() {
    constructor(genres: MutableList<Genre>) : this() {
        this.genres = genres
    }

    @JsonDeserialize(`as` = MutableList::class, contentAs = Genre::class)
    var genres: MutableList<Genre> = mutableListOf()

}