package com.shusharin.fanfictiontomorrow.requests.get_list

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.IGetSearchBook
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.FindBook
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import retrofit2.Call

class ListIdBooksInSectionParams(val section: Sections) :
    MyRequestParams() {
    override fun toString(): String {
        return "ListIdBooksInSectionParams(section=$section)"
    }
}

class GetListIdBooksInSection(
    override val owner: LifecycleOwner, override val api: MyApiServices,
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) : GetListIdBook<ListIdBooksInSectionParams, FindBook>(
    startRefresh,
    stopRefresh
), IGetSearchBook {
    val liveSearchInSectionBook: MutableLiveData<HashMap<Int, FindBook>>
        get() = liveData

    fun sendRequest(section: Sections) {
        data.clear()
        sendRequest(ListIdBooksInSectionParams(section))
    }

    override fun call(params: ListIdBooksInSectionParams): Call<ListBookId> {
        return service.getBooksInSection(params.section.idSection)
    }

    override fun analysisOfResponseBody(
        responseBody: ListBookId,
        params: ListIdBooksInSectionParams,
    ) {
        getSearchBook(idUser, responseBody, params.section.idSection)
    }

    override fun setSearchBook(it: FindBook) {
        setLiveData(it)
    }

}