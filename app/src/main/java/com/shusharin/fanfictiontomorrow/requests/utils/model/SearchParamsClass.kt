package com.shusharin.fanfictiontomorrow.requests.utils.model

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import com.fasterxml.jackson.annotation.JsonProperty
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.databind.ObjectMapper

@JsonIgnoreProperties(ignoreUnknown = true)
class SearchParamsClass {
    @JsonProperty
    var text: String? = null

    @JsonProperty
    var genreId = 0

    @JsonProperty
    var sortingType = 0

    constructor(text: String?, genreId: Int, sortingType: Int) {
        this.text = text
        this.genreId = genreId
        this.sortingType = sortingType
    }

    constructor()


    override fun toString(): String {
        try {
            return ObjectMapper().writerWithDefaultPrettyPrinter().writeValueAsString(this)
        } catch (e: JsonProcessingException) {
            e.printStackTrace()
        }
        return "SearchClass(text=$text, genreId=$genreId, sortingType=$sortingType)"
    }
}