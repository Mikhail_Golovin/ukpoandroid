package com.shusharin.fanfictiontomorrow.requests

import android.graphics.Bitmap
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.io.ByteArrayOutputStream

abstract class RequestCover<RequestType, RequestParams>(
    startRefresh: () -> Unit,
    stopRefresh: () -> Unit,
) : MyRequestIdentity<RequestType, RequestParams>(
    startRefresh, stopRefresh
) {
    protected fun convertToPart(bitmap: Bitmap): MultipartBody.Part {
        val bos = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bos)
        val byteArray: ByteArray = bos.toByteArray()
        val dataPart = RequestBody.create(MediaType.get("multipart/form-data"), byteArray)
        return MultipartBody.Part.createFormData("picture", "cover-", dataPart)
    }
}