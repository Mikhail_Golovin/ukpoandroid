package com.shusharin.fanfictiontomorrow.requests.get_list

import com.shusharin.fanfictiontomorrow.requests.MyRequest
import com.shusharin.fanfictiontomorrow.requests.utils.classes.SmallBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.ListBookId

abstract class GetListIdBook<RequestParams : com.shusharin.fanfictiontomorrow.requests.MyRequestParams, BookType : SmallBook> (
    startRefresh: () -> Unit,
    stopRefresh: () -> Unit,
) : MyRequest<ListBookId, RequestParams, HashMap<Int, BookType>>(startRefresh, stopRefresh) {
    protected val data = HashMap<Int, BookType>()
    fun setLiveData(
        book: BookType,
        isAuthor: Boolean = false,
    ) {
        data[book.id] = book
        if (!isAuthor) {
            if (book.isDraft) {
                data.remove(book.id)
            }
        }
        setLiveData(data)
    }
}