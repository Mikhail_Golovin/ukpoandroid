package com.shusharin.fanfictiontomorrow.requests.delete_queue

import androidx.lifecycle.MutableLiveData
import com.shusharin.fanfictiontomorrow.requests.MyRequestIdentity
import com.shusharin.fanfictiontomorrow.requests.MyRequestParams
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import retrofit2.Call

class DeleteChapterParams(val chapter: Chapter) : MyRequestParams() {
    override fun toString(): String {
        return "DeleteChapterParams(chapter=$chapter)"
    }
}

class DeleteChapter(
    startRefresh: () -> Unit, stopRefresh: () -> Unit,
) :
    MyRequestIdentity<Boolean, DeleteChapterParams>(
        startRefresh,
        stopRefresh
    ) {
    val isDelete: MutableLiveData<Boolean>
        get() = liveData

    fun sendRequest(chapter: Chapter) {
        sendRequest(DeleteChapterParams(chapter))
    }

    override fun call(params: DeleteChapterParams): Call<Boolean> {
        return service.deleteChapter(params.chapter.id)
    }
}