package com.shusharin.fanfictiontomorrow.ui.book

import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class BookViewModel(api: MyApiServices) : ViewModelUpdatable(api) {
    val getBookClass = api.getBookClass()
    val liveBook = getBookClass.liveBook

    val getLastChapter = api.getLastChapter()
    val liveIdLastChapter = getLastChapter.liveIdLastChapter

    val addBookInLibrary = api.addBookInLibrary()
    val liveIsAdded = addBookInLibrary.liveIsAdded

    val setDraft = api.setDraft()
}