package com.shusharin.fanfictiontomorrow.ui.chapters.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.ui.MainActivity

class AdapterChapters(private val chapters: HashMap<Int, Chapter>, private val idBook: Int) :
    RecyclerView.Adapter<AdapterChapters.FindBookViewHolder>() {
    var idLastChapter: Int = -1
    private var MyChapters = Chapter.convertToArrayAndSort(chapters)

    open class FindBookViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameChapter: TextView = itemView.findViewById(R.id.nameBook)
        val youHere: TextView = itemView.findViewById(R.id.youHere)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FindBookViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.content_chapter, parent, false)
        return FindBookViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: FindBookViewHolder, position: Int) {
        val chapter = MyChapters[position].second
        if (idLastChapter == -1) {
            idLastChapter = MyChapters[position].second.id
        }

        holder.nameChapter.text = chapter.name
        if (chapter.id == idLastChapter) {
            holder.youHere.visibility = View.VISIBLE
        } else {
            holder.youHere.visibility = View.GONE
        }
        println(chapter.id)
        holder.nameChapter.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(
                holder.itemView.context.getString(R.string.id_book),
                idBook
            )
            bundle.putInt(
                holder.itemView.context.getString(R.string.id_chapter),
                chapter.id
            )
            MainActivity.navigate(R.id.nav_read_book, bundle)
        }
    }

    override fun getItemCount(): Int {
        return chapters.size
    }

}