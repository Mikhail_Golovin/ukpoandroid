package com.shusharin.fanfictiontomorrow.ui.create_and_edit_book.adapters

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.databinding.FragmentCreateAndEditBinding
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Chapter
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genres
import com.shusharin.fanfictiontomorrow.requests.utils.classes.StatusCreatedBooks
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.create_and_edit_book.CreateAndEditFragment
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogSelectionList
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogYesNo

class AdapterChaptersEdit(
    private val chapters: HashMap<Int, Chapter>,
    private val binding: FragmentCreateAndEditBinding,
    private val genre: DialogSelectionList,
    private val statusCreatedBook: DialogSelectionList,
    override val mySupportFragmentManager :FragmentManager
) :
    RecyclerView.Adapter<AdapterChaptersEdit.FindBookViewHolder>(),
    DialogYesNo.MyDialogYesNoInterface<Chapter> {
    open class FindBookViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val nameChapter: TextView = itemView.findViewById(R.id.nameBook)
        val delete: Button = itemView.findViewById(R.id.delete)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FindBookViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.content_chapter_and_dalete, parent, false)
        return FindBookViewHolder(itemView)
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onBindViewHolder(holder: FindBookViewHolder, position: Int) {
        val chapter = Chapter.convertToArrayAndSort(chapters)[position].second

        holder.nameChapter.text = chapter.name
        holder.nameChapter.setOnClickListener {
            CreateAndEditFragment.book.name = binding.nameBook.text.toString()
            CreateAndEditFragment.book.annotate = binding.annotate.text.toString()

            CreateAndEditFragment.book.genre = Genres.getGenre(genre.getIdSelected())
            CreateAndEditFragment.book.completeness =
                StatusCreatedBooks.getStatusBook(statusCreatedBook.getIdSelected())
            CreateAndEditFragment.book.isDraft = !binding.switchDraft.isChecked
            println(CreateAndEditFragment.book.isDraft)
            val bundle = Bundle()
            bundle.putInt(
                holder.itemView.context.getString(R.string.id_chapter),
                chapter.id
            )
            MainActivity.navController.navigate(R.id.nav_edit_chapter_fragment, bundle)
        }
        val dialogYesNo =
            DialogYesNo(
                "Действительно вы хотите удалить главу «${chapter.name}»?",
                this,
                chapter
            )
        holder.delete.setOnClickListener {
            dialogYesNo.show(mySupportFragmentManager.beginTransaction(), "AdapterChaptersEdit")
        }
    }

    override fun getItemCount(): Int {
        return chapters.size
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun onYes(param: Chapter?) {
        if (param != null) {
            if (!CreateAndEditFragment.addedChapters.containsKey(param.id)) {
                CreateAndEditFragment.deleteChapters[param.id] = param
            }
            chapters.remove(param.id)
            notifyDataSetChanged()
        }
    }

    override fun onNo(param: Chapter?) {
    }


}