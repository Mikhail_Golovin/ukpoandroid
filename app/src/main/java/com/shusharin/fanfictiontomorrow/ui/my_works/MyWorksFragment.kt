package com.shusharin.fanfictiontomorrow.ui.my_works

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.my_works.adapters.AdapterWorkBooks
import com.shusharin.fanfictiontomorrow.ui.parents.RecycleViewFragment

class MyWorksFragment :
    RecycleViewFragment<MyWorksViewModel, AdapterWorkBooks.WorkBookViewHolder, AdapterWorkBooks>() {
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setViewModel<MyWorksViewModel>()
        try {
            viewModel.idAnotherUser = arguments?.getInt(getString(R.string.id_user))!!
            if (viewModel.idAnotherUser == 0) {
                viewModel.idAnotherUser = -1
            }
        } catch (e: java.lang.Exception) {
            viewModel.idAnotherUser = -1
        }

        update()
        refresh()
        return binding.root
    }

    override fun update() {
        viewModel.getListIdWriteBooks.sendRequest(
            if (viewModel.idAnotherUser == -1) {
                MainActivity.idUser
            } else {
                viewModel.idAnotherUser
            }
        )
    }

    override fun addFragmentToStack() {
        MainActivity.stack.addLast(
            MainActivity.Companion.Nav(R.id.nav_my_works, arguments)
        )
    }

    override fun setAdapter() {
        viewModel.liveWriteBooks.observe(viewLifecycleOwner) {
            adapter = AdapterWorkBooks(it)
        }

    }
}