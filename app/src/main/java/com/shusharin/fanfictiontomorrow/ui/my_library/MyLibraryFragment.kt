package com.shusharin.fanfictiontomorrow.ui.my_library

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.ReadBook
import com.shusharin.fanfictiontomorrow.requests.utils.model.SearchParamsClass
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.my_library.adapters.AdapterReedBooks
import com.shusharin.fanfictiontomorrow.ui.parents.RecycleViewFragment
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogFilter
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogSearch
import com.shusharin.fanfictiontomorrow.ui.utils.menu_providers.MenuProviderLibrary

class MyLibraryFragment :
    RecycleViewFragment<MyLibraryViewModel, AdapterReedBooks.ReadBookViewHolder, AdapterReedBooks>(),
    DialogSearch.SearchListener, DialogFilter.FilterListener {
    private lateinit var dialogSearch: DialogSearch
    private lateinit var dialogFilter: DialogFilter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setViewModel<MyLibraryViewModel>()
        try {
            viewModel.idAnotherUser = arguments?.getInt(getString(R.string.id_user))!!
        } catch (e: java.lang.Exception) {
            viewModel.idAnotherUser = -1
        }
        dialogSearch = DialogSearch(childFragmentManager, this)
        dialogFilter = DialogFilter(childFragmentManager, this)

        update()
        refresh()

        requireActivity().addMenuProvider(
            MenuProviderLibrary(
                dialogSearch,
                dialogFilter,
                requireContext()
            ), viewLifecycleOwner
        )

        return binding.root
    }

    override fun update() {
        if (viewModel.isSearch) {
            val searchParamsClass =
                SearchParamsClass(viewModel.text, viewModel.idGenre, viewModel.idSorting)
            viewModel.getListIdSearchBooksInLibrary.sendRequest(
                searchParamsClass,
                idUser()
            )
        } else {
            viewModel.getListIdBooksLibrary.sendRequest(
                idUser()
            )
        }
    }

    private fun idUser() = if (viewModel.idAnotherUser == -1) {
        MainActivity.idUser
    } else {
        viewModel.idAnotherUser
    }

    override fun addFragmentToStack() {
        MainActivity.stack.addLast(
            MainActivity.Companion.Nav(R.id.nav_my_library, arguments)
        )
    }

    override fun setAdapter() {
        viewModel.liveSearchInSectionBook.observe(viewLifecycleOwner) {
            adapter = getAdapter(it)
        }

        viewModel.liveLibraryBook.observe(viewLifecycleOwner) {
            adapter = getAdapter(it)
        }
    }

    private fun filter(readBook: ReadBook): Boolean {
        println("filter ${readBook.statusReadingBook.id} ${readBook.genre.id} ${readBook.completeness.id}")
        if (viewModel.filteredStatusReading != 0
            && viewModel.filteredStatusReading != readBook.statusReadingBook.id
        ) return false

        if (viewModel.filteredGenre != 0
            && viewModel.filteredGenre != readBook.genre.id
        ) return false

        if (viewModel.filteredCompleteness != 0
            && viewModel.filteredCompleteness != readBook.completeness.id
        ) return false

        return true
    }

    override fun searchButtonClicked(bundle: Bundle) {
        viewModel.isSearch = true
        viewModel.text = bundle.getString(getString(R.string.text), "")
        viewModel.idGenre = bundle.getInt(getString(R.string.id_genre), 1)
        viewModel.idSorting = bundle.getInt(getString(R.string.id_sorting), 1)
        update()
    }

    override fun filterApply(idStatusReading: Int, idGenre: Int, idCompleteness: Int) {
        println("MyLibrary $idStatusReading $idGenre $idCompleteness")
        viewModel.filteredStatusReading = idStatusReading
        viewModel.filteredGenre = idGenre
        viewModel.filteredCompleteness = idCompleteness
        adapter = if (viewModel.isSearch) {
            getAdapter(viewModel.liveSearchInSectionBook.value!!)
        } else {
            getAdapter(viewModel.liveLibraryBook.value!!)
        }
    }

    private fun getAdapter(hashMap: HashMap<Int, ReadBook>) =
        try {
            val hashMapCopy = HashMap<Int, ReadBook>()
            hashMap.asSequence().filter { filter(it.value) }
                .forEach { hashMapCopy[it.key] = it.value }
            AdapterReedBooks(hashMapCopy)
        } catch (e: Exception) {
            AdapterReedBooks(HashMap())
        }

}