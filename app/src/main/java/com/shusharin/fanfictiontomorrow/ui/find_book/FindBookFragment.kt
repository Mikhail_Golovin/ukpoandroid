package com.shusharin.fanfictiontomorrow.ui.find_book

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Sections
import com.shusharin.fanfictiontomorrow.requests.utils.model.SearchParamsClass
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.find_book.adapters.AdapterFindBook
import com.shusharin.fanfictiontomorrow.ui.parents.RecycleViewFragment
import com.shusharin.fanfictiontomorrow.ui.utils.WHERE_FIND

class FindBookFragment :
    RecycleViewFragment<FindBookViewModel, AdapterFindBook.FindBookViewHolder, AdapterFindBook>() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?, savedInstanceState: Bundle?,
    ): View {
        super.onCreateView(inflater, container, savedInstanceState)
        setViewModel<FindBookViewModel>()
        viewModel.whereFindId = arguments?.getInt(getString(R.string.where_find))!!
        setAdapter()
        when (viewModel.whereFindId) {
            WHERE_FIND.SECTION.id -> {
                viewModel.idWhereFind = requireArguments().getInt(getString(R.string.id_section))
            }
            WHERE_FIND.GENRE.id -> {
                viewModel.idWhereFind = requireArguments().getInt(getString(R.string.id_genre))
            }
            WHERE_FIND.FIND.id -> {
                viewModel.text = requireArguments().getString(getString(R.string.text)).toString()
                viewModel.idGenre = requireArguments().getInt(getString(R.string.id_genre))
                viewModel.idSorting = requireArguments().getInt(getString(R.string.id_sorting))
            }
        }

        update()
        refresh()
        return binding.root
    }

    override fun update() {
        when (viewModel.whereFindId) {
            WHERE_FIND.SECTION.id -> {
                viewModel.getListIdBooksInSection.sendRequest(Sections.get(viewModel.idWhereFind))
            }
            WHERE_FIND.GENRE.id -> {
                viewModel.getListIdGenreBooks.sendRequest(viewModel.idWhereFind)
            }
            WHERE_FIND.FIND.id -> {
                val searchParamsClass =
                    SearchParamsClass(viewModel.text, viewModel.idGenre, viewModel.idSorting)
                viewModel.getListIdSearchBooks.sendRequest(searchParamsClass)
            }
        }

    }

    override fun addFragmentToStack() {
        MainActivity.stack.addLast(MainActivity.Companion.Nav(R.id.nav_find_book, arguments))
    }

    @SuppressLint("NotifyDataSetChanged")
    override fun setAdapter() {
        when (viewModel.whereFindId) {
            WHERE_FIND.SECTION.id -> {
                viewModel.liveSearchInSectionBook.observe(viewLifecycleOwner) {
                    adapter = AdapterFindBook(it)
                    adapter!!.notifyDataSetChanged()
                }
            }
            WHERE_FIND.GENRE.id -> {
                viewModel.liveSearchInGenreBook.observe(viewLifecycleOwner) {
                    adapter = AdapterFindBook(it)
                    adapter!!.notifyDataSetChanged()
                }
            }
            WHERE_FIND.FIND.id -> {
                viewModel.liveSearchBook.observe(viewLifecycleOwner) {
                    adapter = AdapterFindBook(it)
                    adapter!!.notifyDataSetChanged()
                }
            }
        }

    }
}