package com.shusharin.fanfictiontomorrow.ui.parents

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.ViewModelProvider
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewbinding.ViewBinding
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogSearch
import com.shusharin.fanfictiontomorrow.ui.utils.menu_providers.MenuProviderGeneral
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


abstract class MyViewFragment<ViewModel : ViewModelUpdatable, Binding : ViewBinding> : Fragment() {
    lateinit var viewModel: ViewModel
    protected lateinit var binding: Binding
    private var countRefresh = 0
    protected val executor: ExecutorService = Executors.newSingleThreadExecutor()
    protected val handler = Handler(Looper.getMainLooper())
    protected lateinit var swipeRefreshLayout: SwipeRefreshLayout

    open fun setAdapter() {}
    abstract fun setBinding(inflater: LayoutInflater, container: ViewGroup?)
    protected inline fun <reified R : ViewModel> setViewModel() {
        MainActivity.viewModelStore = viewModelStore
        viewModel = ViewModelProvider(viewModelStore, MyViewModelFactory(MainActivity.api))[R::class.java]
        setAdapter()
    }

    protected abstract fun setSwipeRefreshLayout()
    protected abstract fun addFragmentToStack()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View? {
        countRefresh = 0
        setBinding(inflater, container)
        setSwipeRefreshLayout()
        addFragmentToStack()
        appBar()
        MainActivity.api = MyApiServices(viewLifecycleOwner, resources, this::startRefresh, this::stopRefresh)
        return super.onCreateView(inflater, container, savedInstanceState)
    }

    private fun appBar() {
        requireActivity().addMenuProvider(
            MenuProviderGeneral(
                DialogSearch(parentFragmentManager),
                requireContext()
            ), viewLifecycleOwner
        )
    }

    fun stopRefresh() {
        countRefresh--
        println("countRefresh = $countRefresh")
        if (swipeRefreshLayout.isRefreshing && countRefresh <= 0) {
            countRefresh = 0
            swipeRefreshLayout.isRefreshing = false
        }
    }

    fun startRefresh() {
        countRefresh++
        println("countRefresh = $countRefresh")
        if (!swipeRefreshLayout.isRefreshing) {
            swipeRefreshLayout.isRefreshing = true
        }
    }

    protected open fun refresh() {
        swipeRefreshLayout.setOnRefreshListener {
            update()
        }
    }

    open fun update() {}

    companion object {
        fun appBar(
            activity: FragmentActivity,
            context: Context,
            fragmentManager: FragmentManager,
            owner: LifecycleOwner,
        ) {
            activity.addMenuProvider(
                MenuProviderGeneral(
                    DialogSearch(fragmentManager),
                    context
                ), owner
            )
        }
    }
}