package com.shusharin.fanfictiontomorrow.ui.utils.menu_providers

import android.content.Context
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import androidx.core.view.MenuProvider
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogFilter
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogSearch

class MenuProviderLibrary(private val dialogSearch: DialogSearch, private val dialogFilter: DialogFilter, val context: Context) :
    MenuProvider {
    override fun onCreateMenu(menu: Menu, menuInflater: MenuInflater) {
        menu.clear()
        menuInflater.inflate(R.menu.app_bar_my_library, menu)
    }

    override fun onMenuItemSelected(menuItem: MenuItem): Boolean {
        when (menuItem.itemId) {

            R.id.app_bar_search_ -> {
                dialogSearch.show(
                    dialogSearch.supportFragmentManager.beginTransaction(),
                    "MyLibraryFragment"
                )
                return true
            }

            R.id.app_bar_filter -> {
                dialogFilter.show(
                    dialogFilter.supportFragmentManager.beginTransaction(),
                    "MyLibraryFragment"
                )
                return true
            }

            else -> {
                MainActivity.stack.removeLast()
                MainActivity.navigateBack()
                return true
            }
        }
    }
}