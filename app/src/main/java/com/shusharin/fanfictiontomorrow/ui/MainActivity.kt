package com.shusharin.fanfictiontomorrow.ui

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.view.*
import android.widget.Toast
import androidx.activity.addCallback
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.lifecycle.ViewModelStore
import androidx.navigation.NavController
import androidx.navigation.Navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.NavigationUI.navigateUp
import androidx.navigation.ui.NavigationUI.onNavDestinationSelected
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.navigation.ui.NavigationUI.setupWithNavController
import androidx.preference.PreferenceManager
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.databinding.ActivityMainBinding
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogSearch
import com.shusharin.fanfictiontomorrow.ui.utils.dialogs.DialogYesNo
import com.shusharin.fanfictiontomorrow.utils.Common
import com.shusharin.fanfictiontomorrow.utils.MyApiServices
import com.shusharin.fanfictiontomorrow.utils.RetrofitServices


class MainActivity() : AppCompatActivity(), DialogSearch.SearchListener,
    DialogYesNo.MyDialogYesNoInterface<Any> {
    private lateinit var binding: ActivityMainBinding
    private var mAppBarConfiguration: AppBarConfiguration? = null
    private lateinit var dialogSearch: DialogSearch
    private lateinit var sharedPreferences: SharedPreferences
    private lateinit var dialogYesNo: DialogYesNo<Any>
    override val mySupportFragmentManager = supportFragmentManager
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(
            layoutInflater
        )

        setContentView(binding.root)
        sharedPreferences =
            PreferenceManager.getDefaultSharedPreferences(this)

        dialogYesNo = DialogYesNo(getString(R.string.are_you_sure), this)
        myWindow = window

        menu = binding.navView.menu
        menu.findItem(R.id.nav_main_page).isVisible = true
        menu.findItem(R.id.nav_logout).isVisible = false
        if (sharedPreferences.contains(getString(R.string.id_user))) {
            idUser = sharedPreferences.getInt(getString(R.string.id_user), -1)
            if (idUser != -1) {
                menu.findItem(R.id.nav_login).isVisible = false
                menu.findItem(R.id.nav_logout).isVisible = true
            }
        }

        toolbar = binding.appBarMain.toolbar
        setSupportActionBar(toolbar)
        toolbar.title = title


        val drawer = binding.drawerLayout
        val navigationView = binding.navView

        mAppBarConfiguration = AppBarConfiguration.Builder(
            R.id.nav_main_page,
            R.id.nav_settings,
            R.id.nav_login,
            R.id.nav_genres,
            R.id.nav_my_profile,
            R.id.nav_find_book,
            R.id.nav_book,
            R.id.nav_info,
            R.id.nav_chapters,
        ).setOpenableLayout(drawer).build()
        navController = findNavController(this, R.id.nav_host_fragment_content_main)
        setupActionBarWithNavController(this, navController, mAppBarConfiguration!!)
        mySupportActionBar = supportActionBar!!
        setupWithNavController(navigationView, navController)
        dialogSearch = DialogSearch(supportFragmentManager)

        var idNav: Int = -1
        if (sharedPreferences.contains(getString(R.string.id_nav))) {
            idNav = sharedPreferences.getInt(getString(R.string.id_nav), -1)
        }

        Toast.makeText(
            this, sharedPreferences.getString("open_window_values", "None"), Toast.LENGTH_SHORT
        ).show()

        navigationView.setNavigationItemSelectedListener { menuItem ->
            val id = menuItem.itemId
            //it's possible to do more actions on several items, if there is a large amount of items I prefer switch(){case} instead of if()
            if (id == R.id.nav_logout) {
                dialogYesNo.show(supportFragmentManager.beginTransaction(), "MainActivity")
            } else {
                //This is for maintaining the behavior of the Navigation view
                onNavDestinationSelected(menuItem, navController)
                //This is for closing the drawer after acting on it
            }
            drawer.closeDrawer(GravityCompat.START)
            true
        }

        onBackPressedDispatcher.addCallback(this /* lifecycle owner */) {
            // Back is pressed... Finishing the activity
            val drawerLayout = findViewById<View>(R.id.drawer_layout) as DrawerLayout
            if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
                drawerLayout.closeDrawer(GravityCompat.START)
            } else {
                back()
            }
        }
//        when(idNav){
//            R.id.nav_my_library->{
//                navigate(idNav, Bundle())
//            }
//        }
        if (idNav != R.id.nav_main_page && idNav != -1) {
            try {
                onNavDestinationSelected(menu.findItem(idNav), navController)
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun back() {
        when (navController.currentDestination!!.id) {
            R.id.nav_main_page -> {
                clearStack()
                finish()
            }
            R.id.nav_find_book, R.id.nav_my_profile, R.id.nav_book, R.id.nav_read_book, R.id.nav_settings, R.id.nav_my_library -> {
                stack.removeLast()
                navigateBack()
                //                    println(NEED_ME.stack)
            }
            R.id.nav_create_and_edit, R.id.nav_info -> {
                navigateBack()
            }
            else -> {
                clearStack()
                navigate(R.id.nav_main_page)
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return (navigateUp(navController, mAppBarConfiguration!!) || super.onSupportNavigateUp())
    }


    companion object {
        @SuppressLint("StaticFieldLeak")
        lateinit var toolbar: Toolbar
        lateinit var menu: Menu

        @SuppressLint("StaticFieldLeak")
        lateinit var navController: NavController
        lateinit var myWindow: Window
        lateinit var mySupportActionBar: ActionBar
        val mService: RetrofitServices
            get() = Common.retrofitService
        lateinit var api: MyApiServices
        lateinit var viewModelStore: ViewModelStore
        var stack: ArrayDeque<Nav> = ArrayDeque()

        //должно быть -1
        var idUser: Int = -1

        class Nav(val idFragment: Int, val bundle: Bundle? = null) {
            override fun toString(): String {
                return "Nav(idFragment=$idFragment, bundle=$bundle)"
            }
        }

        fun createNeedLoginDialog(context: Context): AlertDialog.Builder {
            val builder = AlertDialog.Builder(context)
            builder.setTitle("Внимание!")
                .setMessage("Для данного действия необходимо авторизироваться!").setPositiveButton(
                    context.getString(R.string.dialog_ok)
                ) { dialog, _ -> // Закрываем диалоговое окно
                    dialog.cancel()
                }
            return builder
        }

        fun navigate(idFragment: Int) {
            navigate(idFragment, null)
        }

        fun navigate(idFragment: Int, bundle: Bundle?) {
            navController.popBackStack()
            navController.navigate(idFragment, bundle)
        }

        fun navigate(nav: Nav) {
            navController.popBackStack()
            navController.navigate(nav.idFragment, nav.bundle)
        }

        fun navigateBack() {
            navigate(stack.removeLast())
        }

        fun clearStack() {
            if (!stack.isEmpty()) {
                for (i in 1..stack.size) {
                    stack.removeLast()
                }
            }
            stack.addLast(Nav(R.id.nav_main_page))
        }
    }

    override fun searchButtonClicked(bundle: Bundle) {
        navigate(R.id.nav_find_book, bundle)
    }

    override fun onYes(param: Any?) {
        val edit = sharedPreferences.edit()
        edit.putInt(getString(R.string.id_user), -1)
        edit.apply()
        idUser = -1
        menu.findItem(R.id.nav_logout).isVisible = false
        menu.findItem(R.id.nav_login).isVisible = true
        clearStack()
        navigate(R.id.nav_main_page)
    }

    override fun onNo(param: Any?) {
    }

//    override fun filterApply(idStatusReading: Int, idGenre: Int, idCompleteness: Int) {
//        println("MainActivity")
//    }
}