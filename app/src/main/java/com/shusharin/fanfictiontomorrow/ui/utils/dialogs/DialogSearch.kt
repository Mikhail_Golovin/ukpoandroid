package com.shusharin.fanfictiontomorrow.ui.utils.dialogs

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.Genres
import com.shusharin.fanfictiontomorrow.ui.utils.Sorting
import com.shusharin.fanfictiontomorrow.ui.utils.WHERE_FIND

class DialogSearch(
    val supportFragmentManager: FragmentManager,
    private val fragment: Fragment? = null,
) :
    DialogFragment() {
    lateinit var idGenreBooks: Array<String>
    private val nameSortingBooks =
        Array(Sorting.values().size) { i -> Sorting.values()[i].nameSoring }
    lateinit var dialogGenres: DialogSelectionList
    lateinit var myContext: Context
    lateinit var dialogSorting: DialogSelectionList

    interface SearchListener {
        fun searchButtonClicked(bundle: Bundle)
    }

    private lateinit var listener: SearchListener

    override fun onAttach(context: Context) {
        super.onAttach(context)
        myContext = context
        listener = try {
            // Instantiate the ConfirmationListener so we can send events to the host
            if (fragment == null) {
                activity as SearchListener
            } else {
                fragment as SearchListener
            }
        } catch (e: ClassCastException) {
            // The activity doesn't implement the interface, throw exception
            if (fragment == null) {
                throw ClassCastException("$activity must implement SearchListener")
            } else {
                throw ClassCastException("$fragment must implement SearchListener")
            }
        }
    }

    @SuppressLint("InflateParams")
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?,
    ): View {
        idGenreBooks = Array(Genres.values().size) { i -> getString(Genres.values()[i].nameG) }

//        dialog!!.setMessage("Найдётся не всё)!")
        val viewDialog = inflater.inflate(R.layout.content_find_request, null)

        val search = viewDialog.findViewById<Button>(R.id.search)
        val textNameBook = viewDialog.findViewById<EditText>(R.id.text_name_book)

        val nameGenre = viewDialog.findViewById<TextView>(R.id.name_genre)
        nameGenre.text = idGenreBooks[0]
        dialogGenres =
            DialogSelectionList("Выберите жанр", idGenreBooks, nameGenre)


        nameGenre.setOnClickListener {
            dialogGenres.show(supportFragmentManager.beginTransaction(), "dialogGenres")
        }

        val nameSorting = viewDialog.findViewById<TextView>(R.id.name_sorting)
        nameSorting.text = nameSortingBooks[0]
        dialogSorting = DialogSelectionList(
            "Выберите вид сортировки",
            nameSortingBooks,
            nameSorting,
        )
        nameSorting.setOnClickListener {
            dialogSorting.show(supportFragmentManager.beginTransaction(), "dialogSorting")
        }

        search.setOnClickListener {
            if (textNameBook.text.toString() == "") {
                Toast.makeText(
                    myContext,
                    "Ошибка! Поле для ввода названия книги пустое!",
                    Toast.LENGTH_SHORT
                ).show()
            } else {
                val bundle = Bundle()
                bundle.putInt(
                    getString(R.string.where_find),
                    WHERE_FIND.FIND.id
                )
                bundle.putString(
                    getString(R.string.text),
                    textNameBook.text.toString()
                )
                bundle.putInt(
                    getString(R.string.id_genre),
                    dialogGenres.getIdSelected()
                )
                bundle.putInt(
                    getString(R.string.id_sorting),
                    dialogSorting.getIdSelected()
                )
                dismiss()
                listener.searchButtonClicked(bundle)
            }
        }
        return viewDialog
    }
}