package com.shusharin.fanfictiontomorrow.ui.my_works

import com.shusharin.fanfictiontomorrow.ui.parents.ViewModelUpdatable
import com.shusharin.fanfictiontomorrow.utils.MyApiServices

class MyWorksViewModel(api: MyApiServices) : ViewModelUpdatable(api){
    val getListIdWriteBooks = api.getListIdWriteBooks()
    val liveWriteBooks = getListIdWriteBooks.liveWriteBooks
}