package com.shusharin.fanfictiontomorrow.ui.main_page.adapters

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.shusharin.fanfictiontomorrow.R
import com.shusharin.fanfictiontomorrow.requests.utils.classes.FindBook
import com.shusharin.fanfictiontomorrow.ui.MainActivity
import kotlin.math.min

class AdapterSmallBooks(
    private val smallBooks: HashMap<Int, FindBook>,
) :
    RecyclerView.Adapter<AdapterSmallBooks.SmallBookViewHolder>() {

    open class SmallBookViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val coverBook: ImageView = itemView.findViewById(R.id.cover)
        val nameBook: TextView = itemView.findViewById(R.id.nameBook)
        val nameAuthor: TextView = itemView.findViewById(R.id.nameAuthor)
        val cornerCon: View = itemView.findViewById(R.id.cornerCon)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SmallBookViewHolder {
        val itemView =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.content_small_book, parent, false)
        return SmallBookViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: SmallBookViewHolder, position: Int) {
        val temp = smallBooks.toList().toTypedArray()
        temp.sortBy { it.second.sort }
        val smallBook = temp[position]
        holder.coverBook.setImageBitmap(smallBook.second.bitmap)
        holder.nameBook.text = smallBook.second.name
        holder.nameAuthor.text = smallBook.second.author.toString()
        holder.cornerCon.clipToOutline = true

        holder.cornerCon.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(
                holder.itemView.context.getString(R.string.id_book),
                smallBook.second.id
            )
            MainActivity.navigate(R.id.nav_book, bundle)
        }
    }


    override fun getItemCount() = min(smallBooks.size, 15)
}