package com.shusharin.fanfictiontommorow.ui.end_to_end.common;

import android.content.res.Resources;
import android.view.View;

import androidx.recyclerview.widget.RecyclerView;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Objects;

public class DoubleRecyclerViewMatcher {
    private final int recyclerViewId;

    public DoubleRecyclerViewMatcher(int recyclerViewId) {
        this.recyclerViewId = recyclerViewId;
    }


    public Matcher<View> atPositionOnView(final int positionRecycleView, final int recyclerViewInsideId, final int positionInside, final int targetViewId) {

        return new TypeSafeMatcher<>() {
            Resources resources = null;
            View childRecyclerView;
            View childView;

            public void describeTo(Description description) {
                String idDescription = Integer.toString(recyclerViewId);
                if (this.resources != null) {
                    try {
                        idDescription = this.resources.getResourceName(recyclerViewId);
                    } catch (Resources.NotFoundException var4) {
                        idDescription = String.format("%s (resource name not found)",
                                recyclerViewId);
                    }
                }

                description.appendText("with id: " + idDescription);
            }

            public boolean matchesSafely(View view) {

                this.resources = view.getResources();

                if (childRecyclerView == null) {
                    RecyclerView recyclerView =
                            view.getRootView().findViewById(recyclerViewId);
                    if (recyclerView != null && recyclerView.getId() == recyclerViewId) {
                        try {
                            childRecyclerView = Objects.requireNonNull(recyclerView.findViewHolderForAdapterPosition(positionRecycleView)).itemView;
                        } catch (Exception e) {
                            childRecyclerView=null;
                            return false;
                        }
                    } else {
                        return false;
                    }
                }

                RecyclerView targetRecyclerView = childRecyclerView.findViewById(recyclerViewInsideId);

                if (childView == null) {
                    if (targetRecyclerView != null && targetRecyclerView.getId() == recyclerViewInsideId) {
                        try {
                            childView = Objects.requireNonNull(targetRecyclerView.findViewHolderForAdapterPosition(positionInside)).itemView;
                        } catch (Exception e) {
                            childView=null;
                            return false;
                        }

                    } else {
                        return false;
                    }
                }

                if (targetViewId == -1) {
                    return view == childView;
                } else {
                    View targetView = childView.findViewById(targetViewId);
                    return view == targetView;
                }
            }
        };
    }
}
